function handlePost(details, content) {
	let data = JSON.parse(content);
	if (data.extension.mediaInfo !== undefined) {
		let title = data.extension.mediaInfo.title;
		browser.tabs.sendMessage(details.tabId, {
			msgtype: 'title',
			title: title,
			id: data.postId
		});
	}
}

function handleVideo(details, content) {
	let data = JSON.parse(content);
	if (data.videos !== undefined && data.videos.list.length > 0) {
		let videos = [];
		data.videos.list.forEach(function(v) {
			let vid = {};
			vid.format = v.encodingOption.name.toLowerCase();
			vid.url = v.source;
			videos.push(vid);
		});
		videos.sort(function(a, b) {
			let a1 = parseInt(a.format.slice(0, -1));
			let b1 = parseInt(b.format.slice(0, -1));
			return b1-a1;
		});

		let captions = [];

		if (data.captions !== undefined && data.captions.list.length > 0) {
			data.captions.list.forEach(function(c) {
				let cap = {};
				cap.lang = c.label;
				cap.locale = c.locale;
				cap.url = c.source;
				captions.push(cap);
			});
		}

		browser.tabs.sendMessage(details.tabId, {
			msgtype: 'videos',
			videos: videos,
			captions: captions
		});

	}
}

function requestListener(details) {
	if (details.method !== 'GET' && details.method !== 'POST') {
		return;
	}

	let filter = browser.webRequest.filterResponseData(details.requestId);
	let decoder = new TextDecoder("utf-8");
	let data = [];

	filter.ondata = function(event) {
		data.push(event.data);
		filter.write(event.data);
	}

	filter.onstop = function(event) {
		let str = "";
		for (let buffer of data) {
			str += decoder.decode(buffer, {stream: true});
		}
		str += decoder.decode();
		try {
			if (details.url.match(/^https?:\/\/(?:global\.)?apis\.naver\.com\/weverse\/wevweb\/post\/v1\.0\/post-\d+-\d+\?fieldSet=postV1.*$/)) {
				handlePost(details, str);
			} else if (details.url.match(/^https?:\/\/(?:global\.)?apis\.naver\.com\/rmcnmv\/rmcnmv\/vod\/play\/v2\.0\/.*$/)) {
				handleVideo(details, str);
			}
		} catch (e) {
			console.log(e);
		} finally {
			filter.close();
		}
	}
	
	return {};
}

browser.webRequest.onBeforeRequest.addListener(
	requestListener,
	{urls: [
		"*://*.apis.naver.com/weverse/*",
		"*://*.apis.naver.com/rmcnmv/*"
	]},
	["blocking"]
);

browser.runtime.onMessage.addListener(dl);
function dl(msg) {
	let filename = msg.filename.replace(/\/|:|\?|\*|\||\\|"|<|>/g, "_");
	filename = filename.replace(/[^\p{L}\p{N}\p{P}\p{Z}]/gu, "_");
	let downloading = browser.downloads.download({
		url: msg.url,
		filename: filename
	});
}
