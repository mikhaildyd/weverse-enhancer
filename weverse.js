let title = null;
let videos = null;
let captions = null;

function download(url, filename) {
	browser.runtime.sendMessage({"url": url, "filename": filename});
}

function handleMsg(request, sender, sendResponse) {
	if (request.msgtype === 'title') {
		title = request;
		videos = null;
		captions = null;
		delBox();
	} else if (request.msgtype === 'videos') {
		videos = request.videos;
		captions = request.captions;
		addBox();
	}
}

function delBox() {
	let detail_wrap = document.querySelector('div[class^=UpperAreaView_detail_wrap]');
	detail_wrap.style.overflow = 'hidden';
	let boxes = document.querySelectorAll('div.download-box');
	boxes.forEach(function(box) {
		box.remove();
	});
}

function createButton(text, list) {
	let button = document.createElement('div');
	button.setAttribute('class', 'download-button');
	button.textContent = text;
	button.innerHTML += '<svg class="arrow" width="12" height="12" viewBox="0 0 12 12"><g fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round"><g stroke="#979797"><g><g><path d="M.455.455L5 5M9.545.455L5 5" transform="translate(-209 -1709) translate(209 1709) translate(1 3)"></path></g></g></g></g></svg>';

	button.addEventListener("click", function(e) {
		if (list.style.display == "none") {
			list.style.display = "block";
		} else {
		list.style.display = "none";
		}
	});


	return button;
}

function createList() {
	let list = document.createElement('div');
	list.setAttribute('class', 'download-list');
	list.style.display = "none";

	return list;
}

function createItem(text, url, filename) {
	let item = document.createElement('a');
	item.textContent = text;
	item.setAttribute('class', 'download-item');
	item.setAttribute('href', url);

	item.addEventListener("click", function(e) {
		e.preventDefault();
		download(url, filename);
	});

	return item;
}

function addBox() {
	let detail_wrap = document.querySelector('div[class^=UpperAreaView_detail_wrap]');
	detail_wrap.style.overflow = 'visible';
	let header = document.querySelector('div[class^=HeaderView_container]');
	let box = document.createElement('div');
	box.setAttribute('class', 'download-box');
	box.setAttribute('id', 'download-box');

	let sub_box = document.createElement('div');
	sub_box.setAttribute('class', 'download-box');
	sub_box.setAttribute('id', 'subtitles-download-box');

	if (videos !== null) {
		let download_list = createList();
		let download_button = createButton("Download video", download_list);

		videos.forEach(function(v) {
			let div = createItem(v.format, v.url, title.title+" ["+title.id+"].mp4");
			download_list.appendChild(div);
		});
		box.appendChild(download_button);
		box.appendChild(download_list);
		header.parentNode.appendChild(box);
	}

	if (captions !== null && captions.length > 0) {
		let subtitle_list = createList();
		let subtitle_button = createButton("Download subtitles", subtitle_list);
		
		captions.forEach(function(c) {
			let div = createItem(c.lang, c.url, title.title+" ["+title.id+"]."+c.locale+".vtt");
			subtitle_list.appendChild(div);
		});
		sub_box.appendChild(subtitle_button);
		sub_box.appendChild(subtitle_list);
		header.parentNode.appendChild(sub_box);
	}
}

browser.runtime.onMessage.addListener(handleMsg);

window.addEventListener('click', function(e) {
	if (!e.target.matches('.download-button') && !e.target.matches('.download-item') && !e.target.matches('.download-list')) {
		let lists = document.querySelectorAll('.download-list');
		lists.forEach(function(l) {
			if (l.style.display != "none") {
				l.style.display = "none";
			}
		});
	}
});
